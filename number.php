<?php

$servername = "localhost";
$username = "root";
$password = "abb3h5Mv";
$dbname = "chegadeagrotoxicos";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT count(*) FROM ca_dk_speakout_signatures";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo number_format($row['count(*)']);
    }
} else {
    echo "0 results";
}
$conn->close();

?>
