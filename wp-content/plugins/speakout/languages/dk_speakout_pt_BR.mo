��    ;      �      �      �     �     �     �     �     �     �       	         *     2     >     B     R     i     ~     �     �     �  
   �     �     �     �     �     �  	   �     �     �     �     �     �     �     �     
       	   *     4  
   F     Q     U     i     |     �     �     �     �  
   �     �     �     �            	     9   '  ?   a     �     �  	   �  
   �  $  �     
	     *	     .	     2	     F	     M	     ^	  
   t	     	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     
     
     
     
     "
  	   *
     4
     I
     M
     Q
     U
     Y
     i
     
     �
     �
     �
     �
     �
     �
     �
     �
          !     /     C     W     c     y     �     �  	   �     �  5   �  >   �          )  
   H  :   S   Allow custom messages Apr Aug Call to Action City Confirm Email Confirm signatures Confirmed Country Date Signed Dec Display Options Display address fields Display custom field Email Email Address End date Feb First Name Goal Jan Jul Jun Label Last Name Latest Signatures Mar May Nov Oct Petition ID Petition Message Petition Options Petition Title Post Code Read the petition Return URL Sep Set expiration date Set signature goal Share this with your friends: Sharing URL Sign Now Sign the Petition Signature goal Signatures Signatures collected State State / Province Street Street Address Thank you Thank you. Your signature has been added to the petition. This petition has already been signed using your email address. Twitter Message Your signature has been added. signature signatures Project-Id-Version: SpeakUp! Email Petitions
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-03-20 07:06-0700
PO-Revision-Date: 2016-11-29 14:34-0300
Last-Translator: Kreg <Wallace>
Language-Team: DesignKode <speakup@designkode.com>
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: __;_e;esc_attr__;esc_attr_e;esc_html__;esc_html_e;_x;_ex;esc_attr_x;_n;_nx;_n_noop
X-Poedit-Basepath: ../
X-Poedit-SourceCharset: utf-8
X-Generator: Poedit 1.5.4
X-Poedit-SearchPath-0: .
 Permitir Mensagens Customizadas Abr Ago Chamada para Ação Cidade Confirme o Email Confirmar Assinaturas Confirmado País Data de Assinatura Dez Opções de Aparência Mostrar campos de endereço Mostrar Campo Customizável Email Email Data de Término Fev Nome Meta Jan Jul Jun Rótulo Sobrenome Últimas assinaturas Mar Mai Nov Out ID da Petição Mensagem da Petição Opções da Petição Nome da Petição CEP Ler Petição URL de Retorno Set Data de Expiração Meta de Assinaturas Compartilhe com seus amigos: Compartihar URL Assine agora! Assinar a Petição Meta de Assinaturas Assinaturas Assinaturas Coletadas Estado Estado Rua Endereço Obrigado Obrigado! Sua assinatura foi adicionada à petição. Esta petição já foi assinada usando seu endereço de email. Mensagem para Twitter Sua assinatura foi adicionada. assinatura <b>pessoas já assinaram</b>, ajude a chegar a 2 milhões! 