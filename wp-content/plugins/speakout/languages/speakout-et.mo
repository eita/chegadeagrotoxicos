��    c      4  �   L      p     q     �     �  G   �     �      	     	     	     	     	     	     #	  	   1	     ;	     C	     R	     W	     [	     c	     j	     |	     �	     �	     �	     �	     �	     �	     �	     �	     	
  
   
     '
     5
     >
     O
  
   S
     ^
     c
     l
     r
     u
     y
     }
     �
     �
  	   �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
  	   �
     �
     �
       
        )     .     ;     ?  
   H     S     m     �     �     �     �     �     �  	   �  "   �     �     �  	          	             %     +     G     J     M     a     u  
   �     �     �     �     �  �  �     �     �     �  H   �     -     I     P  	   W     a     n     s     z  
   �     �     �     �     �     �     �     �     �            
   0     ;     U     [     a     g     w     �     �     �     �     �     �     �     �     �     �     �     �     �          	               #     '     .     2     5     8     ;     @     C     S     Z     ^  
   g  
   r  
   }     �     �     �     �     �  	   �     �  	   �     �          :     B     I     V     i     n          �     �     �     �     �  
   �     �     �  "   �     �                     3  	   D     N  
   S     ^     b        b   )   :      *   	   \       =       "   1   K          F   7   >   2       H           c   (   @               R       ?               
   &   I   S   .   Q   $                     #                9   0      5   B   <      !           N   G       Y      O   ;   `   P   -                               /   3   [                     Z         '      W       D   U       +   M   ,   4      X   C          a                  6   _                           V   E   ^          A   %       L       T   J   ]   8    Accepted tags: Accepted variables: Add New Add a custom field to the petition form for collecting additional data. Add me to your mailing list Apr Aug Basic Checked City Columns Confirm Email Confirmed Country Custom Message Date Dec Default Delete Display BCC field Display address fields Display custom field Display custom message Display honorific Display honorific field Donate Edit Email Email Address Email Confirmation Email From Email Message End date Enter title here Feb First Name Goal Greeting Hello ID Jan Jul Jun Label Language Last Name Mar May Message Miss Mr Mrs Ms Name No No Title None Nov Oct Petition Petition  Privacy Read or edit the petition Read the petition Return URL Rows Save Changes Sep Settings Signatures SpeakOut! Email Petitions SpeakOut! Petitions Settings State Street Street Address Success Message Support Target Email Thank you Thank you for signing our petition Theme Title Unchecked Yes confirmed disabled items long list (comma separated) no of rows - default = 20 rows - default = 50 signature signatures signatures table use yes your signature Project-Id-Version: SpeakOut! Email Petitions
POT-Creation-Date: 2018-01-11 10:27+1000
PO-Revision-Date: 2018-01-11 10:27+1000
Last-Translator: 123host <info@123host.com.au>
Language-Team: 
Language: et_EE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.0.5
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: speakout-email-petitions.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 Aktsepteeritud: Aktsepteeritud muutujad: Lisa uus Kohandatud välja lisamine avalduse vormile täiendava teabe kogumiseks. Mind lisada oma meililistis Aprill August Põhiline Kontrollitud Linn Veerud Kinnitage e-post Kinnitatud Riik Kohandatud sõnum Kuupäev dets Vaikimisi valik Kustuta Näidake BCC välja Aadressi väljade kuvamine Kuva kohandatud välja Kuva kohandatud sõnum Kuva kraad Kuvatava austavaid välja Toeta Muuda Email E-posti aadress E-posti kinnitamine Saatja e-post E-posti teade Lõppkuupäev Sisesta pealkiri siia veebr Eesnimi Eesmärk Tervitus Tere ID jaan Juuli Juuni Silt Keel Perekonnanimi Märts Mai Sõnum Prl Mr Pr Pr Nimi Ei Pealkiri puudub Puudub nov Oktoober Petitsioon Petitsioon Privaatsus Lugeda või muuta avalduse Loe avalduse Tagasipöördumise URL: Read Salvesta muudatused September Seaded Allkirjad SpeakOut! E-posti Petitsioonid SpeakOut! Petitsioonide seaded Maakond Tänav Tänav, maja Õnnestumise Teade Tugi Eesmärk e-posti Aitäh Täname meie avalduse Teema Pealkiri Kontrollimata Jah kinnitatud keelatud artiklid pikk nimekiri (komadega eraldatud) ei  /  řidad - vaikimisi = 20 ridad - vaikimisi = 50 allkiri allkirju allkirjad laud kasutamine jah teie allkirjad 