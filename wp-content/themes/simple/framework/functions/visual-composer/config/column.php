<?php
/**
 * Visual Composer Column Configuration
 *
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Global var for class
global $qodx_vc_col_config;

// Start Class
if ( ! class_exists( 'QODX_VC_Col_Config' ) ) {
	
	class QODX_VC_Col_Config {

		/**
		 * Main constructor
		 *
		 */
		public function __construct() {

			// Add new parameters to the column
			add_action( 'init', array( $this, 'add_remove_params' ) );

		}

		/**
		 * Adds new params for the VC Columns
		 *
		 */
		public function add_remove_params() {

			// Array of params to add
			$add_params = array();
			
			$add_params['el_id'] = array(
				'type'                          => 'textfield',
				'heading'                       => esc_html__( 'Column ID', 'wt_vcsc' ),
				'param_name'                    => 'el_id',
				'description'                   => sprintf( esc_html__( 'Enter column ID (Note: make sure it is unique and valid according to', 'wt_vcsc' ) . ' <a href="%s" target="_blank">' . esc_html__( 'w3c specification', 'wt_vcsc' ) . '</a>).', 'http://www.w3schools.com/tags/att_global_id.asp' )
			);			
			$add_params['separator'] = array(
				'type'              			=> 'wt_separator',
				'heading'           			=> '',
				'param_name'        			=> 'separator',
				'separator'             		=> 'Background Extended Settings',
				'description'       			=> '',
				'group' 						=> esc_html__( 'WT_VC Extensions', 'wt_vcsc' )
			);
			$add_params['min_height'] = array(
				'type'                  		=> 'wt_range',
				'heading'               		=> esc_html__( 'Minimum Height', 'wt_vcsc' ),
				'param_name'            		=> 'min_height',
				'value'                 		=> '0',
				'min'                   		=> '0',
				'max'                   		=> '2048',
				'step'                  		=> '1',
				'unit'                  		=> 'px',
				'description'           		=> esc_html__( 'Define the minimum height for this column.', 'wt_vcsc' ),
				'group' 						=> esc_html__( 'WT_VC Extensions', 'wt_vcsc' )
			);
			$add_params['style'] = array(
				'type'							=> 'dropdown',
				'heading'						=> esc_html__( 'Column Style', 'wt_vcsc' ),
				'param_name'					=> 'style',
				'value'							=> array(
					esc_html__( 'None', 'wt_vcsc' )     => '',
					esc_html__( 'Bordered', 'wt_vcsc' ) => 'bordered',
					esc_html__( 'Boxed', 'wt_vcsc' )    => 'boxed',
				),
				'group' 						=> esc_html__( 'WT_VC Extensions', 'wt_vcsc' )
			);
			$add_params['shadow'] = array(
				'type'							=> 'checkbox',
				'heading'						=> esc_html__( 'Drop Shadow?', 'wt_vcsc' ),
				'param_name'					=> 'shadow',
				'value'							=> Array( esc_html__( 'Yes please.', 'wt_vcsc' ) => 'yes' ),
				'description'           		=> esc_html__( 'Check this option to add a default shadow to this column.', 'wt_vcsc' ),
				'group' 						=> esc_html__( 'WT_VC Extensions', 'wt_vcsc' )
			);
			$add_params['typography'] = array(
				'type' 							=> 'dropdown',
				'heading' 						=> esc_html__( 'Typography Style', 'wt_vcsc' ),
				'param_name' 					=> 'typography',
				'value' 						=> array(
					esc_html__( 'Dark Text', 'wt_vcsc' )	=> 'dark',
					esc_html__( 'White Text', 'wt_vcsc' )	=> 'light'
				),
				'description' 					=> esc_html__( 'Select typography style.', 'wt_vcsc' ),
				'group' 						=> esc_html__( 'WT_VC Extensions', 'wt_vcsc' )
			);
			$add_params['bck_color'] = array(
				'type'                          => 'colorpicker',
				'heading'                       => esc_html__( 'Background Color', 'wt_vcsc' ),
				'param_name'                    => 'bck_color',
				'description'                   => esc_html__( 'Select background color for this column.', 'wt_vcsc' ),
				'group' 						=> esc_html__( 'WT_VC Extensions', 'wt_vcsc' )
			);
			$add_params['bg_type'] = array(
				'type' 							=> 'dropdown',
				'heading' 						=> esc_html__( 'Background Type', 'wt_vcsc' ),
				'param_name' 					=> 'bg_type',
				'value' 						=> array(
					esc_html__( 'None', 'wt_vcsc' )			  => '',
					esc_html__( 'Simple Image', 'wt_vcsc' )	  => 'image',
					esc_html__( 'Fixed Image', 'wt_vcsc' )	  => 'fixed',
					esc_html__( 'Parallax Image', 'wt_vcsc' ) => 'parallax',
				),
				'admin_label' 					=> true,
				'description' 					=> esc_html__( 'Select background type for this column.', 'wt_vcsc' ),
				'group' 						=> esc_html__( 'WT_VC Extensions', 'wt_vcsc' )
			);
			$add_params['bck_image'] = array(
				'type'							=> 'attach_image',
				'heading'						=> esc_html__( 'Background Image', 'wt_vcsc' ),
				'param_name'					=> 'bck_image',
				'value'							=> '',
				'description'					=> esc_html__( 'Select the background image for your column.', 'wt_vcsc' ),
				'dependency' 					=> array(
					'element' 	=> 'bg_type',
					'value' 	=> array('image', 'fixed', 'parallax')
				),
				'group' 						=> esc_html__( 'WT_VC Extensions', 'wt_vcsc' )
			);
			$add_params['bg_size'] = array(
				'type'                  		=> 'dropdown',
				'heading'               		=> esc_html__( 'Background Image Size', 'wt_vcsc' ),
				'param_name'            		=> 'bg_size',
				'value'                 		=> array(
					esc_html__( 'Full Size Image', 'wt_vcsc' )		=> 'full',
					esc_html__( 'Large Size Image', 'wt_vcsc' )		=> 'large',
					esc_html__( 'Medium Size Image', 'wt_vcsc' )	=> 'medium',
					esc_html__( 'Thumbnail Size Image', 'wt_vcsc' )	=> 'thumbnail',
				),
				'description'           		=> esc_html__( 'Select which image size based on WordPress settings should be used.', 'wt_vcsc' ),
				'dependency' 					=> array(
					'element' 	=> 'bg_type',
					'value' 	=> array('image', 'fixed', 'parallax')
				),
				'group' 						=> esc_html__( 'WT_VC Extensions', 'wt_vcsc' )
			);
			$add_params['bg_position'] = array(
				'type' 							=> 'dropdown',
				'heading' 						=> esc_html__( 'Background Position', 'wt_vcsc' ),
				'param_name' 					=> 'bg_position',
				'value' 						=> array(
					esc_html__( 'Top', 'wt_vcsc' )	  => 'top',
					esc_html__( 'Middle', 'wt_vcsc' ) => 'center',
					esc_html__( 'Bottom', 'wt_vcsc' ) => 'bottom'
				),
				'description' 					=> '',
				'dependency' 					=> array(
					'element' 	=> 'bg_type',
					'value' 	=> array('image', 'fixed', 'parallax')
				),
				'group' 						=> esc_html__( 'WT_VC Extensions', 'wt_vcsc' )
			);
			$add_params['bg_size_standard'] = array(
				'type' 							=> 'dropdown',
				'heading' 						=> esc_html__( 'Background Size', 'wt_vcsc' ),
				'param_name' 					=> 'bg_size_standard',
				'value' 						=> array(
					esc_html__( 'Cover', 'wt_vcsc' ) 	=> 'cover',
					esc_html__( 'Contain', 'wt_vcsc' ) 	=> 'contain',
					esc_html__( 'Initial', 'wt_vcsc' ) 	=> 'initial',
					esc_html__( 'Auto', 'wt_vcsc' ) 	=> 'auto',
				),
				'description' 					=> '',
				'dependency' 					=> array(
					'element' 	=> 'bg_type',
					'value' 	=> array('image', 'fixed', 'parallax')
				),
				'group' 						=> esc_html__( 'WT_VC Extensions', 'wt_vcsc' )
			);
			$add_params['bg_repeat'] = array(
				'type' 							=> 'dropdown',
				'heading' 						=> esc_html__( 'Background Repeat', 'wt_vcsc' ),
				'param_name' 					=> 'bg_repeat',
				'value' 						=> array(
					esc_html__( 'No Repeat', 'wt_vcsc' )	=> 'no-repeat',
					esc_html__( 'Repeat X + Y', 'wt_vcsc' )	=> 'repeat',
					esc_html__( 'Repeat X', 'wt_vcsc' )		=> 'repeat-x',
					esc_html__( 'Repeat Y', 'wt_vcsc' )		=> 'repeat-y'
				),
				'description' 					=> '',
				'dependency' 					=> array(
					'element' 	=> 'bg_type',
					'value' 	=> array('image', 'fixed', 'parallax')
				),
				'group' 						=> esc_html__( 'WT_VC Extensions', 'wt_vcsc' )
			);
			$add_params['border_color'] = array(
				'type'							=> 'colorpicker',
				'class'							=> '',
				'heading'						=> esc_html__( 'Border Color', 'wt_vcsc' ),
				'param_name'					=> 'border_color',
				'value' 						=> '',
				'group' 						=> esc_html__( 'WT_VC Extensions', 'wt_vcsc' )
			);
			$add_params['border_style'] = array(
				'type'							=> 'dropdown',
				'class'							=> '',
				'heading'						=> esc_html__( 'Border Style', 'wt_vcsc' ),
				'param_name'					=> 'border_style',
				'value'							=> array(
					esc_html__( 'Solid', 'wt_vcsc' )	=> 'solid',
					esc_html__( 'Dotted', 'wt_vcsc' )	=> 'dotted',
					esc_html__( 'Dashed', 'wt_vcsc' )	=> 'dashed',
				),
				'group' 						=> esc_html__( 'WT_VC Extensions', 'wt_vcsc' )
			);
			$add_params['border_width'] = array(
				'type'							=> 'textfield',
				'class'							=> '',
				'heading'						=> esc_html__( 'Border Width', 'wt_vcsc' ),
				'param_name'					=> 'border_width',
				'value'							=> '0px 0px 0px 0px',
				'description'					=> wp_kses( __( 'Your border width in pixels. Example: <strong>1px 1px 1px 1px</strong> (top, right, bottom, left).', 'wt_vcsc' ), 'strong' ),
				'group' 						=> esc_html__( 'WT_VC Extensions', 'wt_vcsc' )
			);
			
			// Paddings & Margins
			$add_params['separator_2'] = array(
				'type'              			=> 'wt_separator',
				'heading'           			=> '',
				'param_name'        			=> 'separator_2',
				'separator'             		=> 'Paddings and Margins',
				'description'       			=> '',
				'group' 						=> esc_html__( 'WT_VC Extensions', 'wt_vcsc' )
			);
			$add_params['padding_top'] = array(
				'type'                  		=> 'wt_range',
				'heading'               		=> esc_html__( 'Padding: Top', 'wt_vcsc' ),
				'param_name'            		=> 'padding_top',
				'value'                 		=> '0',
				'min'                   		=> '0',
				'max'                   		=> '250',
				'step'                  		=> '1',
				'unit'                  		=> 'px',
				'description'           		=> '',
				'group' 						=> esc_html__( 'WT_VC Extensions', 'wt_vcsc' )
			);			
			$add_params['padding_bottom'] = array(
				'type'                  		=> 'wt_range',
				'heading'               		=> esc_html__( 'Padding: Bottom', 'wt_vcsc' ),
				'param_name'            		=> 'padding_bottom',
				'value'                 		=> '0',
				'min'                   		=> '0',
				'max'                   		=> '250',
				'step'                  		=> '1',
				'unit'                  		=> 'px',
				'description'           		=> '',
				'group' 						=> esc_html__( 'WT_VC Extensions', 'wt_vcsc' )
			);
			$add_params['padding_left'] = array(
				'type'                  		=> 'wt_range',
				'heading'               		=> esc_html__( 'Padding: Left', 'wt_vcsc' ),
				'param_name'            		=> 'padding_left',
				'value'                 		=> '0',
				'min'                   		=> '0',
				'max'                   		=> '250',
				'step'                  		=> '1',
				'unit'                  		=> 'px',
				'description'           		=> '',
				'group' 						=> esc_html__( 'WT_VC Extensions', 'wt_vcsc' )
			);
			$add_params['padding_right'] = array(
				'type'                  		=> 'wt_range',
				'heading'               		=> esc_html__( 'Padding: Right', 'wt_vcsc' ),
				'param_name'            		=> 'padding_right',
				'value'                 		=> '0',
				'min'                   		=> '0',
				'max'                   		=> '250',
				'step'                  		=> '1',
				'unit'                  		=> 'px',
				'description'           		=> '',
				'group' 						=> esc_html__( 'WT_VC Extensions', 'wt_vcsc' )
			);
			$add_params['margin_top'] = array(
				'type'                  		=> 'wt_range',
				'heading'               		=> esc_html__( 'Margin: Top', 'wt_vcsc' ),
				'param_name'            		=> 'margin_top',
				'value'                 		=> '0',
				'min'                   		=> '-250',
				'max'                   		=> '250',
				'step'                  		=> '1',
				'unit'                  		=> 'px',
				'description'           		=> '',
				'group' 						=> esc_html__( 'WT_VC Extensions', 'wt_vcsc' )
			);
			$add_params['margin_bottom'] = array(
				'type'                  		=> 'wt_range',
				'heading'               		=> esc_html__( 'Margin: Bottom', 'wt_vcsc' ),
				'param_name'            		=> 'margin_bottom',
				'value'                 		=> '0',
				'min'                   		=> '-250',
				'max'                   		=> '250',
				'step'                  		=> '1',
				'unit'                  		=> 'px',
				'description'           		=> '',
				'group' 						=> esc_html__( 'WT_VC Extensions', 'wt_vcsc' )
			);
			
			// Animations	
			$add_params['separator_3'] = array(
				'type'              			=> 'wt_separator',
				'heading'           			=> '',
				'param_name'        			=> 'separator_3',
				'separator'             		=> 'Animations',
				'description'       			=> '',
				'group' 						=> esc_html__( 'WT_VC Extensions', 'wt_vcsc' )
			);	
			$add_params['css_animation'] = array(
				'type'                          => 'dropdown',
				'heading'                       => esc_html__( 'CSS WT Animation', 'wt_vcsc' ),
				'param_name'                    => 'css_animation',
				'value' => array( esc_html__( 'No', 'wt_vcsc' ) => '', esc_html__( 'Hinge', 'wt_vcsc' ) => 'hinge', esc_html__( 'Flash', 'wt_vcsc' ) => 'flash', esc_html__( 'Shake', 'wt_vcsc' ) => 'shake', esc_html__( 'Bounce', 'wt_vcsc' ) => 'bounce', esc_html__( 'Tada', 'wt_vcsc' ) => 'tada', esc_html__( 'Swing', 'wt_vcsc' ) => 'swing', esc_html__( 'Wobble', 'wt_vcsc' ) => 'wobble', esc_html__( 'Pulse', 'wt_vcsc' ) => 'pulse', esc_html__( 'Flip', 'wt_vcsc' ) => 'flip', esc_html__( 'FlipInX', 'wt_vcsc' ) => 'flipInX', esc_html__( 'FlipOutX', 'wt_vcsc' ) => 'flipOutX', esc_html__( 'FlipInY', 'wt_vcsc' ) => 'flipInY', esc_html__( 'FlipOutY', 'wt_vcsc' ) => 'flipOutY', esc_html__( 'FadeIn', 'wt_vcsc' ) => 'fadeIn', esc_html__( 'FadeInUp', 'wt_vcsc' ) => 'fadeInUp', esc_html__( 'FadeInDown', 'wt_vcsc' ) => 'fadeInDown', esc_html__( 'FadeInLeft', 'wt_vcsc' ) => 'fadeInLeft', esc_html__( 'FadeInRight', 'wt_vcsc' ) => 'fadeInRight', esc_html__( 'FadeInUpBig', 'wt_vcsc' ) => 'fadeInUpBig', esc_html__( 'FadeInDownBig', 'wt_vcsc' ) => 'fadeInDownBig', esc_html__( 'FadeInLeftBig', 'wt_vcsc' ) => 'fadeInLeftBig', esc_html__( 'FadeInRightBig', 'wt_vcsc' ) => 'fadeInRightBig', esc_html__( 'FadeOut', 'wt_vcsc' ) => 'fadeOut', esc_html__( 'FadeOutUp', 'wt_vcsc' ) => 'fadeOutUp', esc_html__( 'FadeOutDown', 'wt_vcsc' ) => 'fadeOutDown', esc_html__( 'FadeOutLeft', 'wt_vcsc' ) => 'fadeOutLeft', esc_html__( 'FadeOutRight', 'wt_vcsc' ) => 'fadeOutRight', esc_html__( 'fadeOutUpBig', 'wt_vcsc' ) => 'fadeOutUpBig', esc_html__( 'FadeOutDownBig', 'wt_vcsc' ) => 'fadeOutDownBig', esc_html__( 'FadeOutLeftBig', 'wt_vcsc' ) => 'fadeOutLeftBig', esc_html__( 'FadeOutRightBig', 'wt_vcsc' ) => 'fadeOutRightBig', esc_html__( 'BounceIn', 'wt_vcsc' ) => 'bounceIn', esc_html__( 'BounceInUp', 'wt_vcsc' ) => 'bounceInUp', esc_html__( 'BounceInDown', 'wt_vcsc' ) => 'bounceInDown', esc_html__( 'BounceInLeft', 'wt_vcsc' ) => 'bounceInLeft', esc_html__( 'BounceInRight', 'wt_vcsc' ) => 'bounceInRight', esc_html__( 'BounceOut', 'wt_vcsc' ) => 'bounceOut', esc_html__( 'BounceOutUp', 'wt_vcsc' ) => 'bounceOutUp', esc_html__( 'BounceOutDown', 'wt_vcsc' ) => 'bounceOutDown', esc_html__( 'BounceOutLeft', 'wt_vcsc' ) => 'bounceOutLeft', esc_html__( 'BounceOutRight', 'wt_vcsc' ) => 'bounceOutRight', esc_html__( 'RotateIn', 'wt_vcsc' ) => 'rotateIn', esc_html__( 'RotateInUpLeft', 'wt_vcsc' ) => 'rotateInUpLeft', esc_html__( 'RotateInDownLeft', 'wt_vcsc' ) => 'rotateInDownLeft', esc_html__( 'RotateInUpRight', 'wt_vcsc' ) => 'rotateInUpRight', esc_html__( 'RotateInDownRight', 'wt_vcsc' ) => 'rotateInDownRight', esc_html__( 'RotateOut', 'wt_vcsc' ) => 'rotateOut', esc_html__( 'RotateOutUpLeft', 'wt_vcsc' ) => 'rotateOutUpLeft', esc_html__( 'RotateOutDownLeft', 'wt_vcsc' ) => 'rotateOutDownLeft', esc_html__( 'RotateOutUpRight', 'wt_vcsc' ) => 'rotateOutUpRight', esc_html__( 'RotateOutDownRight', 'wt_vcsc' ) => 'rotateOutDownRight', esc_html__( 'RollIn', 'wt_vcsc' ) => 'rollIn', esc_html__( 'RollOut', 'wt_vcsc' ) => 'rollOut', esc_html__( 'LightSpeedIn', 'wt_vcsc' ) => 'lightSpeedIn', esc_html__( 'LightSpeedOut', 'wt_vcsc' ) => 'lightSpeedOut' ),
				'description' => esc_html__( 'Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'wt_vcsc' ),
				'group' 						=> esc_html__( 'WT_VC Extensions', 'wt_vcsc' )
			);
			$add_params['anim_type'] = array(
				'type'                          => 'dropdown',
				'heading'                       => esc_html__('WT Animation Visible Type', 'wt_vcsc' ),
				'param_name'                    => 'anim_type',
				'value'                         => array( esc_html__('Animate when element is visible', 'wt_vcsc' ) => 'wt_animate_if_visible', esc_html(__('Animate if element is almost visible', 'wt_vcsc')) => 'wt_animate_if_almost_visible' ),
				'description'                   => esc_html__('Select when the type of animation should start for this element.', 'wt_vcsc' ),
				'group' 						=> esc_html__( 'WT_VC Extensions', 'wt_vcsc' )
			);
			$add_params['anim_delay'] = array(
				'type'                          => 'textfield',
				'heading'                       => esc_html__('WT Animation Delay', 'wt_vcsc'),
				'param_name'                    => 'anim_delay',
				'description'                   => esc_html__('Here you can set a specific delay for the animation (miliseconds). Example: \'100\', \'500\', \'1000\'.', 'wt_vcsc'),
				'group' 						=> esc_html__( 'WT_VC Extensions', 'wt_vcsc' )
			);	
				
			$add_params['el_file'] = array(
				'type'                  		=> 'wt_loadfile',
				'heading'               		=> '',
				'param_name'            		=> 'el_file',
				'value'                 		=> '',
				'file_type'             		=> 'js',
				'file_path'             		=> 'wt-visual-composer-extend-element.min.js',
				'param_holder_class'            => 'wt_loadfile_field',
				'description'           		=> '',
				'group' 						=> esc_html__( 'WT_VC Extensions', 'wt_vcsc' ),
			);
			
			// Loop through array and add new params
			foreach( $add_params as $key => $val ) {
				vc_add_param( 'vc_column', $val );
			}

		}
		
	}

}
$qodx_vc_col_config = new QODX_VC_Col_Config();